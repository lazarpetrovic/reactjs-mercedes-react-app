import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import CarList from './components/car_list';

import Gclass from '../src/pics/G.jpg';
import Cclass from '../src/pics/C.jpg';
import CLSclass from '../src/pics/CLS.jpg';
import Eclass from '../src/pics/E63.jpg';
import GLEclass from '../src/pics/GLE.jpg';
import Sclass from '../src/pics/S.jpg';
import MLclass from '../src/pics/ML.jpg';
import MercedesLogo from '../src/pics/mercedes.jpg';


import SelectedCarView from './components/selected_car_view';
import './index.css';
import Header from '../src/components/header';

class App extends Component {
    constructor(props){
        super(props)

        this.state = {
            cars: [{
             manufacturer : "Mercedez Benz",
             id:1,
             picture:Eclass,
             model:"E 63 AMG",
             price:"105000 $",
             power:"603hp",   
             description:"Absurd power and blistering acceleration on par with supercars are not the E63’s only party tricks; thanks to computer magic and an excellent suspension, it’s also quite agile. Its 603-hp 4.0-liter twin-turbo V-8 is a sweetheart engine that sounds badass. A nine-speed automatic and all-wheel drive are standard, as are heated and cooled front seats. Rear cross-traffic alert and parking assist are standard. If the sedan isn’t enough, check out the wagon: it’ll haul your groceries in record time.."      
        },
        {
            manufacturer: "Mercedes Benz",
            id:2,
            picture:Cclass,
            model:"C63 AMG",
            price:"80000 $",
            power:"470hp",
            description:"If you’ve ever wondered what 503 horsepower feels like strapped into a compact luxury car, Mercedes-AMG has the answer. Available as a sedan, coupe, or convertible, the 469-hp C63 and the aforementioned C63 S make up the top of the C-class’s lineup. Both are powered by versions of the same twin-turbo 4.0-liter V-8 engine, which pairs exclusively with a seven-speed automatic and rear-wheel drive. The ride is rough, but with a firm foot on the accelerator you’ll be having too much fun to care.",

        },
        {
            manufacturer:"Mercedes Benz",
            id:3,
            picture:CLSclass,
            model:"CLS 63 AMG", 
            price:"110000 $",
            power:"577hp",
            description:"Whenever you add the letters “AMG” to a Mercedes model, you are sure to get breathtaking performance, and the CLS63 AMG is no exception. As the high-horsepower version of Mercedes’ “four-door coupe,” it offers stylish lines with room for four. Under the hood is a 5.5-liter twin-turbo V-8 that makes a beastly 577 hp and 590 lb-ft with a seven-speed automatic. All-wheel drive (4Matic) is standard, as is an adaptive sport suspension. An all-new model—dubbed CLS53—goes on sale in late 2018.",

        },
        {
            manufacturer:"Mercedes Benz",
            id:4,
            picture:Gclass,
            model:"G 65 AMG",
            price:"220000 $",
            power:"630hp",
            description:"Breathtakingly expensive, hugely powerful, and anachronistic in looks, the G63 and G65 are not for the introverted. With roots going back to 1979, it retains its basic fashionista-meets-Sandinista look. In the G63, a twin-turbo 5.5-liter V-8 makes 563 hp and mates to a seven-speed automatic; all-wheel drive is standard. The G65 gets a twin-turbo 6.0-liter V-12 that makes 621 hp and moves the three-ton SUV to 60 mph in just 5.1 seconds. An all-new G-class goes on sale soon as a 2019 model..",

        },
        {
            manufacturer:"Mercedez Benz",
            id:5,
            picture:GLEclass,
            model:"GLE 63 AMG",
            price:"150 000 $",
            power:"577hp",
            description:"The grin-inducing, AMG-tuned GLE models are sports cars masquerading as luxury crossovers. A 385-hp twin-turbo 3.0-liter V-6 pairs with a nine-speed automatic in the GLE43, while the GLE63 gets a twin-turbo 5.5-liter V-8 that makes 550 hp (577 hp in S trim) and a seven-speed auto; all-wheel drive is standard on all. Handling and braking are surprisingly athletic, too, despite the SUV bodywork. Since it’s a Mercedes, one can expect the usual luxury appointments, but with a hot-rod flavor."
        },
        {
            manufacturer:"Mercedez Benz",
            id:6,
            picture:MLclass,
            model:"ML 63 AMG",
            price:"180 000 $",
            power:"550hp",
            description:"At about twice the cost of a base ML350, the ML63 AMG requires a big outlay to enjoy its prodigious power and exclusivity. The 5.5-liter twin-turbo V-8 makes 516 hp and 550 hp with the optional Performance package. It’s quick, yet isolates driver and passengers from the sensation of speed; the handling limits arrive with little notice. Luxurious finishes befit its price, coddling occupants in leather and wood. Aside from a deliciously mean exhaust note, the ML63 performs with hushed composure.."
        },{
            manufacturer:"Mercedez Benz",
            id:7,
            picture:Sclass,
            model:"S 63 AMG",
            price:"160 000 $",
            power:"620hp",
            description:"These Teutonic chariots mix sumptuous luxury with earthshaking power. Hinting at their potency with huge air intakes, big wheels, and quad tailpipes, the lineup—available as a sedan, coupe, or cabriolet—starts with the 603-hp twin-turbo V-8 and nine-speed automatic in the all-wheel-drive S63 and ends with the 621-hp twin-turbo V-12 in the S65 that sends a whopping 738 lb-ft to the rear wheels only. The cabins are awash in leather, carbon fiber, and all the trappings of a modern Mercedes."
        }
    ],
        selectedCar:{
            manufacturer:"",
            picture:MercedesLogo,
            description:"Here you can checkout the latest models of Mercedes AMG's"
        }
    };
   
    }

    render() {
        return (
          <div>
       <Header carView = {this.state.selectedCar}/>   
        <SelectedCarView carView = {this.state.selectedCar} />
        <CarList automobil = {this.state.cars}
                onCarSelect = {selectedCar => this.setState({selectedCar})}/> 

          </div>
        );
    };
}




ReactDOM.render(<App /> , document.getElementById("root"));