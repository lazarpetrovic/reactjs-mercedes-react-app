import React from 'react';

const CarListItem = ({onCarSelect,car}) => {
console.log();
    return (
    <li onClick={() => onCarSelect(car)} className="list-group-item lista">
    <div className="media lista">
        <div className="media-left">
          <img src={car.picture} className="media-object" width="180" height="100" alt="rim"/>
        </div>
    </div>  
    </li>
    );
};

export default CarListItem;