import React from 'react';
import CarListItem from './car_list_item';

const CarList = (props) => {
   const carsItems  =  props.automobil.map((car)=> {
    return (
    <CarListItem 
    car = {car}
    key = {car.id}
    onCarSelect ={props.onCarSelect}/>
    
        );
    });
    return (  
     <ul className="col-md-3 col-lg-3 col-sm-3 list-group ">
        {carsItems}
     </ul>
    );
};

export default CarList;