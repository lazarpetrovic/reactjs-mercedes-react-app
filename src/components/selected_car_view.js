import React from 'react';

const SelectedCarView = ({carView}) => {
  console.log(carView);
return (
<div>
  <div className="row kolona" >
      <div className="col-md-3 col-lg-6 img-responsive"> <img src={carView.picture} alt="Selectedcar" height="380" width="580" className="selectedCar img-responsive"/></div>
        <div className="col-md-9 col-lg-6 selectedCarInfo text-justify">
        <div>Mercedes Benz</div>
        <div>{carView.model}</div>
        <div>{carView.power}</div>
        <div>{carView.price}</div>
      <div>{carView.description}</div>
    </div>
  </div>
</div>

);
};

export default SelectedCarView;